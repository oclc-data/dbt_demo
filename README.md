# dbt_demo


# Prerequisites
 You need to have Docker and Git installed in your machine

## Getting started

1. Create .dbt folder in your home directory and create profiles.yml. The below link has the information to update in the profiles.yml .
      
      https://confluence.oclc.org/display/DP/DBT+Container 

2.  Open terminal or command prompt and goto DBT_DEMO location

  3. run the below commands

    1. docker-compose up -d
     
        This command will create the container using the information we provided in docker-compose.yml
    
    2. docker ps 
        
        This command will show the container instances which is running currently. 
        You can see your dbt_demo container in the list. The image name will be dbt_demo_dbt_image

    3. docker exec -it <<your imagename> /bin/bash
        
        This command will open the container with working directory we specifed in the docker-compose.

    4. dbt debug

        This will test  the database connection and show information.
    
    5. dbt run
       
        This command will run all the models and provide the result.
    
    6. dbt test

        This command will run the test we defined against all the models.

    7. dbt docs generate

         This will generate the documentation about the metadata of all the models we created.

    8. dbt docs serve
     
        The generated document will be served to http://localhost:8080
